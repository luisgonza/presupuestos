
class campos_n_crossovered_budget(models.Model):# modelo el cual hace un inherit al modelo crossovered.budget
    _inherit = 'crossovered.budget'

    file_import  = fields.Binary(string='Archivo de importación')
    total_budget = fields.Monetary(string="Total del presupuesto")
    record_numbers = fields.Integer(string="Número de registros")
    imported_registration_numbers = fields.Integer(string="Número de registros importados")
   # budget_import  este es un button con el nombre Importar presupuesto 
    budget_of_project_dgpo = fields.Boolean(string="Presupuesto de DGPO",required=True)
    move_id = fields.Many2one('account.move',string="Asiento contable")

class campos_n_crossovered_budget_lines(models.Model):# modelo en el cual se hace un inherit al modedlo existente crossovered.budget.lines pag del doc 24
    _inherit = 'crossovered.budget.lines'

    authorized_amount = fields.Float(string="Autorizado")# en comentario dice si  falta checar aque se refiere este campo
    amount_allocate = fields.Float(string="Asignado",digits=(12,2))
    amount_committed = fields.Float(string="Comprometido",digits=(12,2))
    accrued_amount = fields.Float(string="Devengado",digits=(12,2))
    amount_available = fields.Float(string="Por ejercer",digits=(12,2))
    amount_paid = fields.Float(string="Pagado",digits=(12,2))
    amount_applied = fields.Float(string="Ejercido",digits=(12,2))
    amount_modified = fields.Float(string="Modificado",digits=(12,2))
    amount_available = fields.Float(string="Disponible",digits=(12,2))
    programmatic_account = fields.Text(string="Código programático")#El valor debe estar separado por (.) por cada segmento del código programático
    branch_id = fields.Many2one('branch',string="Dependencia")
    subdependence_id = fields.Many2one('budget.subdependence',string="Subdependencia")
    program_id = fields.Many2one('budget.program',string="Programa")
    subprogram_id = fields.Many2one('budget.subprogram',string="Subprograma")
    item_id = fields.Many2one(' budget.item',string="Partida")
    resource_origin_id = fields.Many2one('budget.resource.origin',string="Origen del recurso")
    institutional_activity_id = fields.Many2one('budget.institutional.activity',string="Actividad institucional")
    conpp_id = fields.Many2one('budget.program.conversion',string="Conversión de programa presupuestario")
    conpa_id = fields.Many2one('budget.item.conversion',string="Conversión con partida")
    expense_type_id = fields.Many2one('budget.expense.type',string="Tipo de gasto")
    geographic_location_id = fields.Many2one('budget.geographic.location',string="Ubicación geográfica")
    key_portfolio_id = fields.Many2one('budget.key.portfolio',string="Clave cartera")

class budget_amount_allocated(models.Model):# modelo para Control de montos asignados. pag 24 doc
    _name = 'budget.amount.allocated'

    # checar el documento porque hay botones y barras de state asi mismo como la estructura

    code = fields.Text(string="Folio",required=True)
    budget_id = fields.Many2one('crossovered.budget',string="Presupuesto",required=True)
    description = fields.Text(string="Observaciones")
    date_import = fields.Date(string="Fecha de importación",required=True)
    file_amount_allocated  = fields.Binary(string="Archivo estacionalidad",required=True)
    user_id = fields.Many2one('res.users',string="Realizado por",required=True)
    reason_for_rejection = fields.Text(string="Motivo del rechazo")
    #Dentro de una pestaña llamada Control de depósitos se agregan los siguientes campos
    
   # currency_id = fields.Many2one('') se dice q hay q generar un id pero falta 
    assigment_amount = fields.Monetary(string="Monto asignado",digits=(12,2),required=True)
    deposit_date = fields.Date(string="Fecha depósito")
    deposit_amount = fields.Monetary(string="Monto depositado",required=True,readonly=True)
    deposit_account_bank_id = fields.Many2one('account.journal',string="Cuenta del depósito")
    pending_amount = fields.Monetary(string="Monto pendiente",digits=(12,2),readonly=True)
    Comments = fields.Text(string="Observaciones")
    move_id = fields.Many2one('account.move',string="Asiento contable") #requerido  Sí Cuando el estado del documento es aprobado


class budget_amount_allocated_lines(models.Model):#modelo para para guardar los valores ybudget.amount.allocated.lines
    _name = 'budget.amount.allocated.lines'
    
    
access_budget_amount_allocated,budget.amount.allocated,model_budget_amount_allocated,,1,1,1,1
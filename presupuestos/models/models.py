# -*- coding: utf-8 -*-

from odoo import models, fields, api 
from datetime import datetime, date, time, timedelta
from odoo.exceptions import ValidationError



class budget_program(models.Model):
    _name = 'budget.program'

    code =  fields.Char(string='Código',required=True,size=2)
    name = fields.Char(string="Nombre",required=True)
    budget_subprogam_id = fields.Many2one('budget.subprogram',string="Subprograma")

class budget_subprogram(models.Model):
#se crea este modelo  SubPrograma (SP) con los siguientes campos 
    _name = 'budget.subprogram'

    code =  fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)
    branch_id = fields.Many2one('res.branch',string="Dependencia",required=True,)
    subdependence_id = fields.Many2one('budget.subdependence',string="Subdependencia",required=True)
    program_id = fields.Many2one('budget.program',string="Programa",required=True)

class campos_nuevos_branch(models.Model):#Este es un inherit al modelo del branch ,Dependencia (DEP).
    _inherit = 'res.branch'

    code = fields.Char(string="Código",required=True,size=3)
    head = fields.Many2one('hr.employee',string="Titular",required=True)
    secretary = fields.Many2one('hr.employee',string="Secretario Administrativo",required=True)
    No_dependence = fields.Char(string="No. entidad o dependencia",required=True)
    subdependence_id = fields.Many2one('budget.subdependence',string="Subdependencias")

class  budget_subdependence(models.Model):#Modelo Subdependencia (SD)
    _name = 'budget.subdependence'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)
    branch_id = fields.Many2one('res.branch',string="Dependencia",required=True,)

class budget_item(models.Model):#Modelo para Partida de Gasto (PAR).
    _name = 'budget.item'

    code = fields.Char(string="Partida",required=True,size=3)
    name = fields.Char(string="Nombre",required=True)
    type_item = fields.Selection([('Regulada','R'),('Centralizada','C'),('Directa','D')],string="Tipo de ejercicio",required=True)
    description = fields.Char(string="Descripción")
    cogconac_id  = fields.Many2one('budget.cog.conac',string="COG CONAC",required=True)
    cogshcp_id = fields.Many2one('budget.item.conversion',string="COG SHCP",required=True)
    expense_account = fields.Many2one('account.account',string="Cuenta de gastos",required=True)
    debtor_account = fields.Many2one('account.account', string="Cuenta pasivo deudora",required=True)

class budget_resource_origin(models.Model):#Modelo para Origen del Recurso(OR).
    _name = 'budget.resource.origin'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)
    is_income_owne = fields.Boolean(string='Si')
    is_income_owne2 = fields.Boolean(string='No')
    observations = fields.Char(string="Observaciones")

class budget_institutional_activity(models.Model):# Modelo para Actividad Institucional(AI).
    _name = 'budget.institutional.activity'

    code = fields.Char(string="Código",required=True,size=5)
    name = fields.Char(string="Nombre",required=True)

class budget_program_conversion(models.Model):#modelo para Conversión de Programa Presupuestario(CONPP).
    _name = 'budget.program.conversion'

    code = fields.Char(string="Código",required=True,size=4)
    name = fields.Char(string="Nombre",required=True)
    cogshcp_id = fields.Many2one('budget.item.conversion',string="PP SHCP",required=True)
    activity = fields.Selection([('bachillerato','Bachillerato'),('licenciatura','Licenciatura'),('posgrado','Posgrado'),('becas','Becas'),('cultura','Cultura'),('mantenimiento','Mantenimiento'),('obras','Obras')],string="Actividad",required=True)

class budget_item_conversion(models.Model):#modelo para Conversión con partida (CONPA).
    _name = 'budget.item.conversion'

    code = fields.Char(string="Código (partida)",required=True,size=5)
    name = fields.Char(string="Nombre",required=True)
    item_number = fields.Many2one('budget.item',string="Partida")
    type_item = fields.Selection([('Regulada','R'),('Centralizada','C'),('Directa','D')],string="Tipo de ejercicio",required=True)
    cogconac_id = fields.Many2one('budget.cog.conac',string="COG CONAC",required=True)

class budget_expense_type(models.Model):# modelo para Tipo de gasto (TG).
    _name = 'budget.expense.type'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)

class budget_geographic_location(models.Model):#modelo para Ubicación Geográfica (UG).
    _name = 'budget.geographic.location'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)

class budget_key_portfolio(models.Model):#modelo Clave cartera(CC).
    _name = 'budget.key.portfolio'

    code = fields.Char(string="Código",required=True,size=4)
    name = fields.Char(string="Nombre",required=True)
    description = fields.Char(string="Descripción")
    entity_id = fields.Many2one('budget.geographic.location',string="Entidad",required=True)
    type_program = fields.Selection([('1','Proyecto de Inversión de Infraestructura social'),('2','Programa de Inversión de Mantenimiento'),('3','Programa de Inversión de Adquisiciones')],string="Tipo de programa o proyecto",required=True)

class budget_project_type(models.Model):#modelo para el Tipo de proyecto(TP).
    _name = 'budget.project.type'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)

class project_project_Modificar(models.Model):# modelo para Proyectos (NP). haciendo inherit al modelo project.project
    _inherit = 'project.project'


    #code = fields.Char(string="Código",required=True, size=5, related="campo_relacion.campo_a_traer" )#aki falta lo relacionado al account.analityc.accoun
    type_project_id = fields.Many2one('budget.project.type',string="Tipo de proyecto",required=True)
    sub_type = fields.Selection([('spp','(SPP) Sistema de pagos a proveedores de bienes y prestadores de servicio.'),('cbc','(CBC) Cuenta bancaria con chequera.')])
    amount_allocated = fields.Monetary(string="Monto asignado",required=True)
    consumed_amount = fields.Monetary(string="Monto ejercido",required=True)

class budget_stage(models.Model):#modelo para Etapa(E).
    _name = 'budget.stage'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)

class  budget_agreement_type(models.Model):#modelo para Tipo de convenio(TC).
    _name = 'budget.agreement.type'

    code = fields.Char(string="Código",required=True,size=2)
    name = fields.Char(string="Nombre",required=True)

class agreement_agreement(models.Model):#modelo para Convenios(NC)
    _name = 'agreement.agreement'

    code = fields.Char(string="Código",required=True,size=6)
    name = fields.Char(string="Nombre",required=True)
    agreement_type_id = fields.Many2one('budget.agreement.type',string="Tipo de convenio",required=True)
    branch_id = fields.Many2one('branch',string="Dependencia",required=True)
    budget_subdependence_id = fields.Many2one('budget.subdependence',string="Subdepencencia",required=True)


class budget_cog_conac(models.Model):#modelo para Catálogo COG CONAC ()
    _name = 'budget.cog.conac'

    code = fields.Char(string="Código",required=True)
    name = fields.Char(string="Nombre",required=True)

class budget_structure(models.Model):#modelo para Orden del código programático.
    _name = 'budget.structure'

    sequence = fields.Integer(string="Secuencia",required=True)
    name = fields.Char(string="Nombre",required=True)
    catalog_id = fields.Many2one('budget.cog.conac',string="catalog_id")
    position_from  = fields.Integer(string='Posición inicial',required=True)
    position_to = fields.Integer(string="Posición final",required=True)

class c_nuevos_asientos_contables(models.Model):#modelo para Asientos contables,el cual hace un inherit al modelo  account.move.lines agregando los siguientes campos
    _inherit ='account.move.line'

    branch_id = fields.Many2one('branch',string="Dependencia")
    subdependence_id = fields.Many2one('budget.subdependence',string="Subdependencia")
    program_id = fields.Many2one('budget.program',string="Programa")
    subprogram_id = fields.Many2one('budget.subprogram',string="Subprograma")
    item_id = fields.Many2one('budget.item',string="Partida")
    resource_origin_id = fields.Many2one('budget.resource.origin',string="Origen del recurso")
    institutional_activity_id = fields.Many2one('budget.institutional.activity',string="Actividad institucional")
    conpp_id = fields.Many2one('budget.program.conversion',string="Conversión de programa presupuestario")
    conpa_id = fields.Many2one('budget.item.conversion',string="Conversión con partida")
    expense_type_id = fields.Many2one('budget.expense.type',string="Tipo de gasto")
    geographic_location_id = fields.Many2one('budget.geographic.location',string="Ubicación geográfica")
    key_portfolio_id = fields.Many2one('budget.key.portfolio',string="Clave cartera")
    type_project_id =fields.Many2one('budget.project.type',string="Tipo de proyecto")
    project_number_id = fields.Many2one('project.project',string="Número de proyecto")













 